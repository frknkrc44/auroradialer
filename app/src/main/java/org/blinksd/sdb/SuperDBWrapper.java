package org.blinksd.sdb;

import android.content.Context;

public class SuperDBWrapper {

    public static SuperMiniDB getDefaultDB(Context context) {
        return new SuperMiniDB(context.getPackageName(), context.getFilesDir());
    }

}
