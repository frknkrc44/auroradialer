package com.aurora.phone;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutActivity extends BaseActivity {

    @BindView(R.id.line1)
    AppCompatTextView title;
    @BindView(R.id.line2)
    AppCompatTextView subtitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setTitle(R.string.settings_about_app_intent);
        ButterKnife.bind(this);
        findViewById(R.id.action_backward).setOnClickListener((v) -> onBackPressed());
        fillAppDetails();
    }

    private void fillAppDetails() {
        try {
            PackageManager pm = getPackageManager();
            PackageInfo ai = pm.getPackageInfo(getPackageName(), 0);
            title.setText(ai.applicationInfo.loadLabel(pm));
            subtitle.setText(ai.versionName);
        } catch (Throwable t) {
            // do nothing
        }

        String cs = String.format("%s<br><br>%s", subtitle.getText(), getString(R.string.about_description).replaceAll("\n", "<br>"));
        subtitle.setMovementMethod(LinkMovementMethod.getInstance());
        subtitle.setText(Html.fromHtml(cs.trim()));
    }
}