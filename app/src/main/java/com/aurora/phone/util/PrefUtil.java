/*
 * Aurora Droid
 * Copyright (C) 2019, Rahul Kumar Patel <whyorean@gmail.com>
 *
 * Aurora Droid is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Aurora Droid is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Aurora Droid.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.aurora.phone.util;

import android.text.TextUtils;

import com.aurora.phone.AuroraApplication;

import org.blinksd.sdb.SuperMiniDB;

import java.util.ArrayList;
import java.util.Arrays;

public class PrefUtil {

    public static void putString(String key, String value) {
        SuperMiniDB smdb = AuroraApplication.getDB();
        smdb.putString(key, value);
        smdb.refreshKey(key);
    }

    public static void putInteger(String key, int value) {
        putString(key, String.valueOf(value));
    }

    public static void putFloat(String key, float value) {
        putString(key, String.valueOf(value));
    }

    public static void putBoolean(String key, boolean value) {
        putString(key, String.valueOf(value));
    }

    public static void putListString(String key, ArrayList<String> stringList) {
        String[] myStringList = stringList.toArray(new String[stringList.size()]);
        putString(key, TextUtils.join("‚‗‚", myStringList));
    }


    public static String getString(String key) {
        return getString(key, null);
    }

    private static String getString(String key, Object def) {
        SuperMiniDB smdb = AuroraApplication.getDB();
        return smdb.getString(key, String.valueOf(def));
    }

    public static int getInteger(String key) {
        return getInteger(key, 0);
    }

    public static int getInteger(String key, int def) {
        return Integer.parseInt(getString(key, def));
    }

    public static float getFloat(String key) {
        return Float.parseFloat(getString(key, 0.0f));
    }

    public static Boolean getBoolean(String key) {
        return Boolean.parseBoolean(getString(key, false));
    }

    public static ArrayList<String> getListString(String key) {
        return new ArrayList<String>(Arrays.asList(TextUtils.split(getString(key, ""), "‚‗‚")));
    }
}
