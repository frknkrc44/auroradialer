package com.aurora.phone.util;

import android.app.Activity;
import android.app.UiModeManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class ViewUtil {

    public static void setFullScreenLightStatusBar(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = activity.getWindow().getDecorView().getSystemUiVisibility();
            if(!isNightModeEnabled(activity))
                flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            flags |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            flags |= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            activity.getWindow().getDecorView().setSystemUiVisibility(flags);
        }
    }

    public static void configureActivityLayout(Activity activity) {
        Window window = activity.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.flags &= WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        window.setAttributes(params);
        window.setStatusBarColor(Color.TRANSPARENT);
        ViewUtil.setFullScreenLightStatusBar(activity);
    }

    public static int getStyledAttribute(Context context, int styleID) {
        TypedArray arr = context.obtainStyledAttributes(new TypedValue().data, new int[]{styleID});
        int styledColor = arr.getColor(0, Color.WHITE);
        arr.recycle();
        return styledColor;
    }

    public static boolean isNightModeEnabled(Context context) {
        switch (PrefUtil.getInteger(SettingUtil.THEME_SELECTOR, THEME_SYSTEM)){
            case THEME_SYSTEM:
                UiModeManager uiModeMgr = (UiModeManager) context.getSystemService(Context.UI_MODE_SERVICE);
                return uiModeMgr.getNightMode() == UiModeManager.MODE_NIGHT_YES;
            case THEME_LIGHT:
                return false;
            case THEME_DARK:
                return true;
        }

        Log.e("Night mode value invalid");
        return false;
    }

    private static final int THEME_SYSTEM = 0, THEME_LIGHT = 1, THEME_DARK = 2;
}
