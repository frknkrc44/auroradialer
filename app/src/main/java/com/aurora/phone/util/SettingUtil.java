package com.aurora.phone.util;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aurora.phone.AboutActivity;
import com.aurora.phone.sheet.SettingsMenu;
import com.aurora.phone.view.SettingsIntent;
import com.aurora.phone.view.SettingsRadioButtonDialog;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Simple settings backend implementation
 */
public class SettingUtil {
    private Map<String, Type> settingMap = new LinkedHashMap<>();

    public SettingUtil() {
        // Add settings
        settingMap.put(THEME_SELECTOR, Type.RADIO_BUTTON);
        settingMap.put(ABOUT_APP_INTENT, Type.INTENT);
    }

    public ViewGroup getSettingView(SettingsMenu menu) {
        LinearLayout layout = new LinearLayout(menu.getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        for(String item : settingMap.keySet()) {
            switch(Objects.requireNonNull(settingMap.get(item))) {
                case RADIO_BUTTON:
                    SettingsRadioButtonDialog radioButtonDialog = new SettingsRadioButtonDialog(menu, item);
                    layout.addView(radioButtonDialog.getItemView(layout));
                    break;
                case TEXT_INPUT:
                case SEEK_BAR:
                case CHECKBOX_LIST:
                case SWITCH:
                    break;
                case INTENT:
                    SettingsIntent settingsIntent = new SettingsIntent(menu, item);
                    layout.addView(settingsIntent.getItemView(layout));
                    break;
            }
        }
        return layout;
    }

    /**
     * Get radio button or checkbox strings
     * Responds with null if something wrong
     *
     * @param context
     * @param key
     * @return String[] or null
     */
    @Nullable
    public String[] getSelectorItems(Context context, String key) {
        if(!settingMap.containsKey(key))
            return null;

        Resources res = context.getResources();
        try {
            int resId = res.getIdentifier(String.format("settings_array_%s", key), "array", context.getPackageName());
            if (resId > 0)
                return res.getStringArray(resId);
        } catch(Throwable t) {
            // do nothing
        }

        return null;
    }

    /**
     * Get radio button selected value
     * Returns -1 if something wrong
     *
     * @param key
     * @return int
     */
    public int getDefaultRadioButtonValues(String key) {
        switch(key) {
            case THEME_SELECTOR:
                return Defaults.THEME_SELECTOR;
        }
        return -1;
    }

    public Intent getIntent(Context context, String key) {
        switch (key) {
            case ABOUT_APP_INTENT:
                return new Intent(context, AboutActivity.class);
        }
        return null;
    }

    /**
     * Get translation about settings
     *
     * @param context
     * @param key
     * @return String
     */
    @NonNull
    public String getTranslation(Context context, String key) {
        String transKey = String.format("settings_%s", key);
        if(!settingMap.containsKey(key))
            return transKey;

        Resources res = context.getResources();
        try {
            int resId = res.getIdentifier(transKey, "string", context.getPackageName());
            if (resId > 0)
                return res.getString(resId);
        } catch(Throwable t) {
            // do nothing
        }

        return transKey;
    }

    /**
     * Using for settings type detection
     */
    public enum Type {
        SWITCH, /* Adds a switch */
        CHECKBOX_LIST, /* Opens a dialog, contains checkboxes */
        RADIO_BUTTON, /* Opens a dialog, contains radio buttons */
        TEXT_INPUT, /* Opens a dialog, contains a text input */
        SEEK_BAR, /* Opens a dialog, contains a seek bar */
        INTENT, /* Triggers a intent if user clicked */
        /* I can change here at future */
    }

    public static final String THEME_SELECTOR = "theme_selector",
                                ABOUT_APP_INTENT = "about_app_intent";

    private static class Defaults {
        public static int THEME_SELECTOR = 0;
    }
}
