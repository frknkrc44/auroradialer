package com.aurora.phone.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.IBinder;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.telecom.Call;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;

import com.aurora.phone.Constants;
import com.aurora.phone.MainActivity;
import com.github.florent37.runtimepermission.PermissionResult;
import com.github.florent37.runtimepermission.rx.RxPermissions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.disposables.CompositeDisposable;

public class Util {

    public static int getStatusBarHeight(Activity context) {
        Resources res = context.getResources();
        int resId = res.getIdentifier("status_bar_height", "dimen", "android");
        return resId > 0
                ? res.getDimensionPixelSize(resId)
                : (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25, res.getDisplayMetrics());
    }

    public static void setContactsAvailable(boolean value) {
        PrefUtil.putBoolean(Constants.PREFERENCE_CONTACTS_AVAILABLE, value);
    }

    public static boolean isContactsAvailable() {
        return PrefUtil.getBoolean(Constants.PREFERENCE_CONTACTS_AVAILABLE);
    }

    public static boolean checkDefaultDialer(Context context) {
        String packageName = context.getPackageName();
        if (!context.getSystemService(TelecomManager.class).getDefaultDialerPackage().equals(packageName)) {
            Intent intent = new Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER)
                    .putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, packageName);
            ((MainActivity) context).startActivityForResult(intent, 1337);
            return false;
        }
        return true;
    }

    public static void checkPermissions(Context context) {
        CompositeDisposable disposable = new CompositeDisposable();
        disposable.add(new RxPermissions((MainActivity) context)
                .request(Manifest.permission.READ_CONTACTS,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.CALL_PHONE)
                .subscribe(result -> {
                }, throwable -> {
                    final PermissionResult result = ((RxPermissions.Error) throwable).getResult();
                    if (result.hasDenied()) {
                        for (String permission : result.getDenied()) {
                            Log.e("User denied : %s", permission);
                        }
                        result.askAgain();
                    }

                    if (result.hasForeverDenied()) {
                        for (String permission : result.getForeverDenied()) {
                            Log.e("User permanently denied : %s", permission);
                        }
                        result.goToSettings();
                    }
                }));
    }

    public static void toggleSoftInput(Context context, boolean show) {
        IBinder windowToken = ((MainActivity) context).getWindow().getDecorView().getWindowToken();
        InputMethodManager inputMethodManager = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && windowToken != null)
            if (show)
                inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            else
                inputMethodManager.hideSoftInputFromWindow(windowToken, 0);
    }

    public static synchronized void vibrate(@NonNull Context context, long millis) {
        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (vibrator == null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(millis, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(millis);
        }
    }

    public static synchronized void playTone(@NonNull Context context, String numStr) {
        try {
            ToneGenerator toneGenerator = new ToneGenerator(AudioManager.STREAM_DTMF, 80);
            AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            int ringerMode = audioManager.getRingerMode();
            int tone;
            if (numStr.equals("*"))
                tone = 11;
            else if (numStr.equals("#"))
                tone = 12;
            else
                tone = Integer.parseInt(numStr);
            if ((ringerMode == AudioManager.RINGER_MODE_SILENT) || (ringerMode == AudioManager.RINGER_MODE_VIBRATE)) {
                return;
            }
            toneGenerator.startTone(tone, 150);
        } catch (Exception ignored) {

        }
    }

    public static long getDiffInDays(long timeInMilli) {
        try {
            long duration = Calendar.getInstance().getTimeInMillis() - timeInMilli;
            return TimeUnit.MILLISECONDS.toDays(duration);
        } catch (Exception e) {
            return 1;
        }
    }

    public static String getTimeFromMilli(long timeInMilli) {
        try {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
            return simpleDateFormat.format(new Date(timeInMilli));
        } catch (Exception e) {
            return "NA";
        }
    }

    public static String getDateFromMilli(long timeInMilli) {
        try {
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM YY, hh:mm a", Locale.getDefault());
            return simpleDateFormat.format(new Date(timeInMilli));
        } catch (Exception e) {
            return "NA";
        }
    }

    public static long getMilliFromDate(String data, long Default) {
        try {
            return Date.parse(data);
        } catch (Exception e) {
        }
        return Default;
    }

    public static String formatSecs(long secs){
        int sec = (int)(secs % 60);
        int min = (int)((secs / 60) % 60);
        int hour = (int)((secs / 60) / 60);

        return getFormattedTime(hour, min, sec);
    }

    public static String formatMillis(long millis){
        return formatSecs(millis / 1000);
    }

    @SuppressLint("DefaultLocale")
    private static String getFormattedTime(int hour, int min, int sec) {
        String out = "";

        if (hour > 0){
            out += String.format("%02d:", hour);
        }

        out += String.format("%02d:%02d", min, sec);

        return out;
    }

    public static void copyToClipboard(Context context, String text) {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText(context.getPackageName(), text);
        clipboardManager.setPrimaryClip(clipData);
    }
    
    public static int getSIMType(Context context) {
        SubscriptionManager manager = (SubscriptionManager) context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
        if (manager != null && context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            List<SubscriptionInfo> list = manager.getActiveSubscriptionInfoList();
            if (list.size() >= 2)
                return SimType.DUAL;
            else if (list.size() == 1)
                return SimType.SINGLE;
            else
                return SimType.NONE;
        }
        return SimType.NONE;
    }
    
    public static class SimType {
        private SimType() {} // don't init
        
        public static final int SINGLE = 1,
                                DUAL   = 2,
        /* I didn't see yet */  TRI    = 3,
                                NONE   = 0;
    }

    public static boolean isIncoming(Call call) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            return call.getDetails().getCallDirection() == Call.Details.DIRECTION_INCOMING;
        return call.getState() == Call.STATE_RINGING;
    }
}
