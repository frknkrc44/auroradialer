package com.aurora.phone.util;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.telecom.Call;

import com.aurora.contact.entity.Contact;
import com.aurora.phone.OnGoingCallActivity;
import com.aurora.phone.R;

public class NotificationUtil {

    public static void getCallNotification(Service context, Call call) {
        Notification.Builder builder = new Notification.Builder(context);
        builder.setAutoCancel(false);
        builder.setContentTitle(call.getDetails().getCallerDisplayName());
        builder.setContentText(String.format("%s call", Util.isIncoming(call) ? "Incoming" : "Outgoing"));
        builder.setSmallIcon(R.drawable.ic_call);
        Intent intent = new Intent(context, OnGoingCallActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        builder.setContentIntent(pendingIntent);
        String channelId = context.getPackageName() + ".Call";
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = notificationManager.getNotificationChannel(channelId);
            if(channel == null) {
                channel = new NotificationChannel(channelId, "Dialer Call", NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
            }
            builder.setChannelId(channelId);
        }
        context.startForeground(channelId.hashCode(), builder.build());
    }

    public static void getMissedCallNotification(Context context, int count, String phoneNumber) {
        Contact contact = ContactUtils.getContactByPhoneNumber(context, phoneNumber);
        if(contact == null) return;
        Notification.Builder builder = new Notification.Builder(context);
        builder.setAutoCancel(false);
        builder.setContentTitle(String.format("%s missed call(s)", String.valueOf(count)));
        builder.setContentText(count > 1 ? "" : contact.getCompositeName());
        builder.setSmallIcon(R.drawable.ic_call_missed);
        String channelId = context.getPackageName() + ".MissedCall";
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = notificationManager.getNotificationChannel(channelId);
            if(channel == null) {
                channel = new NotificationChannel(channelId, "Dialer Missed Call", NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
            }
            builder.setChannelId(channelId);
        }
        notificationManager.notify(channelId.hashCode(), builder.build());
    }

}
