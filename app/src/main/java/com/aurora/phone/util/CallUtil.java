package com.aurora.phone.util;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.provider.CallLog;

public class CallUtil {

    public static void markAllMissedCallLogsAsRead(Context context) {
        markMissedCallLogsAsRead(context, null);
    }

    // https://android.googlesource.com/platform/packages/apps/Dialer/+/master/java/com/android/dialer/app/calllog/CallLogNotificationsQueryHelper.java
    public static void markMissedCallLogsAsRead(Context context, Uri callUri) {
        ContentValues values = new ContentValues();
        values.put(CallLog.Calls.NEW, 0);
        values.put(CallLog.Calls.IS_READ, 1);
        StringBuilder where = new StringBuilder();
        where.append(CallLog.Calls.NEW);
        where.append(" = 1 AND ");
        where.append(CallLog.Calls.TYPE);
        where.append(" = ?");
        try {
            context
                    .getContentResolver()
                    .update(
                            callUri == null ? CallLog.Calls.CONTENT_URI : callUri,
                            values,
                            where.toString(),
                            new String[] {Integer.toString(CallLog.Calls.MISSED_TYPE)});
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

}
