package com.aurora.phone.service;

import android.content.Intent;
import android.os.PowerManager;
import android.telecom.Call;
import android.telecom.InCallService;

import com.aurora.phone.AuroraApplication;
import com.aurora.phone.OnGoingCallActivity;
import com.aurora.phone.util.NotificationUtil;
import com.aurora.phone.util.Util;

import java.util.Objects;

public class CallService extends InCallService {

    @Override
    public void onCallAdded(Call call) {
        super.onCallAdded(call);
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        NotificationUtil.getCallNotification(this, call);
        if(!powerManager.isInteractive() || !Util.isIncoming(call)) {
            Intent intent = new Intent(this, OnGoingCallActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        AuroraApplication.call.add(call);
    }

    @Override
    public void onCallRemoved(Call call) {
        super.onCallRemoved(call);
        AuroraApplication.call.remove(call);
        if(AuroraApplication.call.size() < 1) {
            stopForeground(true);
        } else {
            NotificationUtil.getCallNotification(this, Objects.requireNonNull(AuroraApplication.getLastCall()));
        }
    }
}
