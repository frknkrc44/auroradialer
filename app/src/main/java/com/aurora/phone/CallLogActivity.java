package com.aurora.phone;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aurora.phone.adapter.CallLogDetailsSection;
import com.aurora.phone.entity.CallLog;
import com.aurora.phone.manager.CallManager;
import com.aurora.phone.util.ContactUtils;
import com.aurora.phone.util.Util;
import com.aurora.phone.util.Util.SimType;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

public class CallLogActivity extends BaseActivity {

    public static CallLog callLog;

    @BindView(R.id.action_text)
    AppCompatTextView actionText;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.img)
    AppCompatImageView img;
    @BindView(R.id.line1)
    AppCompatTextView line1;
    @BindView(R.id.line2)
    AppCompatTextView line2;
    @BindView(R.id.action_call_sim1)
    AppCompatImageView actionCall1;
    @BindView(R.id.action_call_sim2)
    AppCompatImageView actionCall2;
    @BindView(R.id.action_sms)
    AppCompatImageView actionSms;
    @BindView(R.id.action_info)
    AppCompatImageView actionInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        setContentView(R.layout.activity_history_details);
        ButterKnife.bind(this);
        onNewIntent(getIntent());

        if (callLog == null) {
            finishAfterTransition();
            return;
        }

        updateUI(callLog);
        setupRecycler(callLog);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            int contactId = intent.getIntExtra("CONTACT_ID", -1);
            String contactNumber = intent.getStringExtra("CONTACT_NUMBER");

        }
    }

    private void updateUI(CallLog callLog) {
        actionText.setText("Call details");
        // img.setAnimation(AnimationUtils.loadAnimation(this, R.anim.anim_circular_grow));
        if (TextUtils.isEmpty(callLog.getCachedName())) {
            line1.setText(callLog.getNumber());
            line2.setText("");
            actionInfo.setVisibility(View.GONE);
        } else {
            line1.setText(callLog.getCachedName());
            line2.setText(callLog.getNumber());
        }

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (manager != null) {
            switch(Util.getSIMType(this)) {
                case SimType.SINGLE:
                    actionCall2.setVisibility(View.GONE);
                    break;
                case SimType.DUAL:
                case SimType.TRI:
                    // do nothing because visible as default
                    break;
                default:
                    actionCall1.setVisibility(View.GONE);
                    actionCall2.setVisibility(View.GONE);
                    break;
            }
        }
    }

    private void setupRecycler(CallLog callLog) {
        List<CallLog> callLogList = new ArrayList<>();
        callLogList.add(callLog);
        callLogList.addAll(callLog.getRelatedLogs());
        SectionedRecyclerViewAdapter viewAdapter = new SectionedRecyclerViewAdapter();
        viewAdapter.addSection(new CallLogDetailsSection(this, callLogList, "Recent calls", null));
        recycler.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recycler.setAdapter(viewAdapter);
    }

    @Override
    protected void onDestroy() {
        img.setAnimation(null);
        callLog = null;
        super.onDestroy();
    }

    @OnClick(R.id.action_backward)
    @Override
    public void onBackPressed(){
        super.onBackPressed();
    }

    @OnClick(R.id.action_call_sim1)
    public void actionCallSIM1() {
        CallManager.call(this, getContactNumber(false), 1);
    }

    @OnClick(R.id.action_call_sim2)
    public void actionCallSIM2() {
        CallManager.call(this, getContactNumber(false), 2);
    }

    @OnClick(R.id.action_sms)
    public void actionSMS() {
        ContactUtils.sendSMSEvent(this, getContactNumber());
    }

    @OnClick(R.id.action_info)
    public void actionInfo() {
        Intent infoIntent = new Intent(this, ContactActivity.class);
        infoIntent.putExtra("CONTACT_NUMBER", getContactNumber());
        startActivity(infoIntent);
    }

    private String getContactNumber() {
        return getContactNumber(true);
    }

    private String getContactNumber(boolean includeTel) {
        String out = includeTel ? "tel:" : "";
        if(callLog != null) {
            out += callLog.getNumber();
        } else if (getIntent() != null) {
            out += getIntent().getStringExtra("CONTACT_NUMBER");
        }
        return out;
    }

}
