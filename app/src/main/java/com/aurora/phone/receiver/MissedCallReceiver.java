package com.aurora.phone.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.aurora.phone.util.NotificationUtil;

public class MissedCallReceiver extends BroadcastReceiver {

    public static final String ACTION_SHOW_MISSED_CALLS_NOTIFICATION =
            "android.telecom.action.SHOW_MISSED_CALLS_NOTIFICATION";
    public static final String EXTRA_NOTIFICATION_COUNT = "android.telecom.extra.NOTIFICATION_COUNT";
    public static final String EXTRA_NOTIFICATION_PHONE_NUMBER =
            "android.telecom.extra.NOTIFICATION_PHONE_NUMBER";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(!ACTION_SHOW_MISSED_CALLS_NOTIFICATION.equals(intent.getAction())) {
            return;
        }

        int count = intent.getIntExtra(EXTRA_NOTIFICATION_COUNT, -1);
        String phoneNumber = intent.getStringExtra(EXTRA_NOTIFICATION_PHONE_NUMBER);

        NotificationUtil.getMissedCallNotification(context, count, phoneNumber);
    }
}