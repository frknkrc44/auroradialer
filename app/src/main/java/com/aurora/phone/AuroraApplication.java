/*
 * Aurora Store
 * Copyright (C) 2019, Rahul Kumar Patel <whyorean@gmail.com>
 *
 * Aurora Store is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Aurora Store is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Aurora Store.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */

package com.aurora.phone;

import android.app.Application;
import android.telecom.Call;

import com.aurora.phone.events.Event;
import com.aurora.phone.events.RxBus;
import com.aurora.phone.util.SettingUtil;

import org.blinksd.sdb.SuperDBWrapper;
import org.blinksd.sdb.SuperMiniDB;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.plugins.RxJavaPlugins;

public class AuroraApplication extends Application {

    public static List<Call> call = new ArrayList<>();
    private static SuperMiniDB sdb;
    private static SettingUtil settingUtil;
    private static RxBus rxBus;

    public static RxBus getRxBus() {
        return rxBus;
    }

    public static void rxNotify(Event event) {
        rxBus.getBus().accept(event);
    }

    public static SettingUtil getSettingUtil() {
        return settingUtil;
    }

    public static Call getLastCall() {
        if(call.size() > 0)
            return call.get(call.size()-1);
        return null;
    }

    public static void mergeCalls() {
        if(call.size() < 2) {
            return;
        }

        for(int i = 1;i < call.size();i++) {
            Call item = call.get(i);
            if(item.getDetails().hasProperty(Call.Details.PROPERTY_CONFERENCE))
                call.get(0).conference(item);
        }
    }

    public static void splitCalls() {
        if(call.size() < 2) {
            return;
        }

        for(int i = 0;i < call.size();i++) {
            Call item = call.get(i);
            if(item.getDetails().hasProperty(Call.Details.PROPERTY_CONFERENCE))
                item.splitFromConference();
        }
    }

    public static SuperMiniDB getDB() {
        return sdb;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        rxBus = new RxBus();
        sdb = SuperDBWrapper.getDefaultDB(this);
        settingUtil = new SettingUtil();
        RxJavaPlugins.setErrorHandler(Throwable::printStackTrace);
    }
}
