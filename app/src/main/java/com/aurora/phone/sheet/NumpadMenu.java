package com.aurora.phone.sheet;

import android.content.Context;
import android.os.Bundle;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aurora.phone.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTouch;

public class NumpadMenu extends BottomSheetDialogFragment {

    @BindView(R.id.navigation_view)
    NavigationView navigationView;
    @BindView(R.id.txt_input_number)
    TextInputEditText txtNumber;

    private Call call;

    public static NumpadMenu getInstance() {
        return new NumpadMenu();
    }
    
    public void setCall(Call call) {
        this.call = call;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.menu_bottom, container, false);
        ViewGroup nav = view.findViewById(R.id.navigation_view);
        inflater.inflate(R.layout.view_numpad_layout, nav);
        View addCall = view.findViewById(R.id.action_add);
        addCall.setVisibility(View.INVISIBLE);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnTouch({R.id.key0, R.id.key1, R.id.key2, R.id.key3, R.id.key4, R.id.key5, R.id.key6, R.id.key7,
            R.id.key8, R.id.key9, R.id.key10, R.id.key11})
    public void onNumpadTouch(View v, MotionEvent event) {
        switch(event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                char numStr = getCharFromId(v.getId());
                int pos = txtNumber.getSelectionEnd();
                StringBuffer number = new StringBuffer(txtNumber.getEditableText());
                number.insert(pos, numStr + "");
                txtNumber.setText(number);
                txtNumber.setSelection(pos + 1);
                call.playDtmfTone(numStr);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                call.stopDtmfTone();
                break;
        }
    }
    
    @OnClick(R.id.action_backspace)
    public void deleteSingleChar() {
        int pos = txtNumber.getSelectionEnd();
        if (pos <= 0)
            return;
        StringBuffer number = new StringBuffer(txtNumber.getEditableText());
        number.deleteCharAt(pos - 1);
        txtNumber.setText(number);
        txtNumber.setSelection(pos - 1);
    }

    @OnLongClick(R.id.action_backspace)
    public void deleteAll() {
        txtNumber.setText("");
    }
    
    private char getCharFromId(int id) {
        switch(id) {
            case R.id.key1: return '1';
            case R.id.key2: return '2';
            case R.id.key3: return '3';
            case R.id.key4: return '4';
            case R.id.key5: return '5';
            case R.id.key6: return '6';
            case R.id.key7: return '7';
            case R.id.key8: return '8';
            case R.id.key9: return '9';
            case R.id.key0: return '0';
            case R.id.key10: return '*';
            case R.id.key11: return '#';
        }
        return 0;
    }
    
}
