package com.aurora.phone.sheet;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aurora.phone.AuroraApplication;
import com.aurora.phone.R;
import com.aurora.phone.util.SettingUtil;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import butterknife.ButterKnife;

public class SettingsMenu extends BottomSheetDialogFragment {

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    public static SettingsMenu getInstance() {
        return new SettingsMenu();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.menu_bottom, container, false);
        ViewGroup nav = view.findViewById(R.id.navigation_view);
        nav.addView(AuroraApplication.getSettingUtil().getSettingView(this));
        ButterKnife.bind(this, view);
        return view;
    }

}
