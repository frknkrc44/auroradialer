package com.aurora.phone;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import static com.aurora.phone.util.ViewUtil.isNightModeEnabled;

public class BaseActivity extends AppCompatActivity {
    boolean firstNightModeSet = false;

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        onConfigurationChanged();
    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);
        TextView textView = findViewById(R.id.action_text);
        if(textView != null) {
            textView.setText(titleId);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        TextView textView = findViewById(R.id.action_text);
        if(textView != null) {
            textView.setText(title);
        }
    }

    private void onConfigurationChanged() {
        onConfigurationChanged(getResources().getConfiguration());
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        boolean nightMode = isNightModeEnabled(this);
        setTheme(nightMode ? R.style.AppTheme_Dark : R.style.AppTheme);
        if(firstNightModeSet) recreate();
        else firstNightModeSet = true;
        super.onConfigurationChanged(newConfig);
    }
}
