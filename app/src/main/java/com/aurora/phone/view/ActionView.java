package com.aurora.phone.view;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.Message;
import android.telecom.Call;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.aurora.phone.AuroraApplication;
import com.aurora.phone.R;
import com.aurora.phone.ViewActionHandler;
import com.aurora.phone.sheet.NumpadMenu;
import com.aurora.phone.util.Log;
import com.aurora.phone.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.AUDIO_SERVICE;

public class ActionView extends ConstraintLayout implements ViewActionHandler {

    @BindView(R.id.action_record)
    Action recordAction;
    @BindView(R.id.action_swap)
    Action swapAction;
    @BindView(R.id.action_merge)
    Action mergeAction;

    private AudioManager audioManager;
    private Context context;
    private Call call;
    private int colorNumber = 0;
    private NumpadMenu numpadMenu;
    private Callback callback = new Callback();
    private MediaRecorder callRecorder;

    // Add for testing
    private Handler recordTimeHandler;

    public ActionView(Context context) {
        super(context);
    }

    public ActionView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.call = AuroraApplication.getLastCall();
        call.registerCallback(callback);
        this.audioManager = (AudioManager) context.getSystemService(AUDIO_SERVICE);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_action, this);
        ButterKnife.bind(this, view);
        if(call.getDetails().hasProperty(Call.Details.PROPERTY_CONFERENCE)) {
            swapAction.setVisibility(View.VISIBLE);
            mergeAction.setVisibility(View.VISIBLE);
        }
    }

    public int getColorNumber() {
        return colorNumber;
    }

    public void setColorNumber(int colorNumber) {
        this.colorNumber = colorNumber;
    }

    @OnClick(R.id.action_pause)
    public void pauseCall(Action action) {
        action.setChecked(!action.isChecked(), getColorNumber());
        if (action.isChecked())
            call.hold();
        else
            call.unhold();
    }

    @OnClick(R.id.action_mute)
    public void muteCall(Action action) {
        if(isCallNotActive()) {
            return;
        }
        action.setChecked(!action.isChecked(), getColorNumber());
        // new Thread(() -> {
            audioManager.setMode(AudioManager.MODE_IN_CALL);
            audioManager.setMicrophoneMute(action.isChecked());
        // }).start();
    }

    @OnClick(R.id.action_speaker)
    public void toggleSpeaker(Action action) {
        if(isCallNotActive()) {
            return;
        }
        action.setChecked(!action.isChecked(), getColorNumber());
        // new Thread(() -> {
            audioManager.setMode(AudioManager.MODE_IN_CALL);
            audioManager.setSpeakerphoneOn(action.isChecked());
        // }).start();
    }

    private boolean isCallNotActive() {
        return call.getState() != Call.STATE_ACTIVE || isCallNotStarted();
    }

    private boolean isCallNotStarted() {
        return call.getDetails().getConnectTimeMillis() <= 0;
    }

    @OnClick(R.id.action_record)
    public void recordCall(Action action) {
        if(isCallNotStarted()) {
            return;
        }
        if(callRecorder == null) {
            callRecorder = new MediaRecorder();
            callRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);
            callRecorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
            callRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            callRecorder.setAudioSamplingRate(44100);
            callRecorder.setOutputFile(getContext().getFilesDir() + "/record_"+ System.currentTimeMillis() +".ogg");
        }
        if(recordTimeHandler == null) {
            recordTimeHandler = new RecordTimeHandler();
        }
        if(!action.isChecked()) {
            startRecord();
        } else {
            stopRecord();
        }
        action.setChecked(!action.isChecked(), getColorNumber());
    }

    private void startRecord() {
        if (callRecorder == null) {
            return;
        }

        try {
            callRecorder.prepare();
            callRecorder.start();
        } catch(Throwable t) {
            return;
        }
        recordTimeHandler.sendEmptyMessage(RecordTimeHandler.MSG_UPDATE_RECORDING);
    }

    private void stopRecord() {
        if (callRecorder == null) {
            return;
        }

        recordTimeHandler.sendEmptyMessage(RecordTimeHandler.MSG_STOP_UPDATE_RECORDING);
        try {
            callRecorder.stop();
            callRecorder.reset();
            callRecorder.release();
        } catch(Throwable t) {

        }
    }

    @OnClick(R.id.action_add)
    public void addCall(Action action) {
        action.setChecked(!action.isChecked(), getColorNumber());
    }

    @OnClick(R.id.action_keypad)
    public void showKeypad(Action action) {
        if(isCallNotActive()) {
            return;
        }
        if (numpadMenu == null) {
            numpadMenu = NumpadMenu.getInstance();
            numpadMenu.setCall(call);
        }
        numpadMenu.show(((AppCompatActivity) context).getSupportFragmentManager(), "FAVOURITE_MENU");
    }
    
    @OnClick(R.id.action_swap)
    public void swapCalls(Action action) {
        if(isCallNotStarted()) {
            return;
        }
        call.swapConference();
    }
    
    @OnClick(R.id.action_merge)
    public void mergeCalls(Action action) {
        if(isCallNotStarted()) {
            return;
        }
        action.setChecked(!action.isChecked(), getColorNumber());
        if(action.isChecked()) {
            AuroraApplication.mergeCalls();
        } else {
            AuroraApplication.splitCalls();
        }
    }

    @Override
    public void start() {

    }

    @Override
    public void onCreate() {

    }

    @Override
    public void stop() {
        if (call != null)
            call.unregisterCallback(callback);
    }
    
    public void updateUI(int state) {
        setVisibility(isCallNotStarted() ? View.INVISIBLE : View.VISIBLE);
        switch (state) {
            case Call.STATE_DIALING:
                break;
            case Call.STATE_ACTIVE:
                break;
            case Call.STATE_DISCONNECTED:
                stopRecord();
                if(numpadMenu != null)
                    numpadMenu.dismiss();
                break;
            case Call.STATE_RINGING:
                break;
            case Call.STATE_CONNECTING:
                break;
            case Call.STATE_HOLDING:
                break;
            default:
                break;
        }
    }

    public class Callback extends Call.Callback {
        @Override
        public void onStateChanged(Call call, int state) {
            super.onStateChanged(call, state);
            Log.e("CallerView : State changed -> %s", state);
            updateUI(state);
        }

        @Override
        public void onDetailsChanged(Call call, Call.Details details) {
            super.onDetailsChanged(call, details);
            Log.i("Details changed: %s", details.toString());
        }
    }

    private class RecordTimeHandler extends Handler {
        private static final int MSG_UPDATE_RECORDING = 0, MSG_STOP_UPDATE_RECORDING = 1;

        private long savedTime = -1;

        @Override
        public void handleMessage(Message msg){
            switch(msg.what){
                case MSG_UPDATE_RECORDING:
                    if(call.getDetails().getConnectTimeMillis() > call.getDetails().getCreationTimeMillis()) {
                        if(savedTime < 0) {
                            savedTime = System.currentTimeMillis();
                        }
                        recordAction.txt.setText(Util.formatMillis(System.currentTimeMillis() - savedTime));
                    }
                    removeMessages(MSG_UPDATE_RECORDING);
                    sendEmptyMessageDelayed(MSG_UPDATE_RECORDING, 500);
                    break;
                case MSG_STOP_UPDATE_RECORDING:
                    removeMessages(MSG_UPDATE_RECORDING);
                    savedTime = -1;
                    recordAction.txt.setText("Record");
                    removeMessages(MSG_STOP_UPDATE_RECORDING);
                    break;
            }
        }

    }
}
