package com.aurora.phone.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aurora.phone.AuroraApplication;
import com.aurora.phone.sheet.SettingsMenu;
import com.aurora.phone.util.PrefUtil;
import com.aurora.phone.util.SettingUtil;

import org.blinksd.sdb.SuperMiniDB;

import java.util.Objects;

public class SettingsRadioButtonDialog extends SettingsBaseDialog {

    public SettingsRadioButtonDialog(SettingsMenu menu, String key) {
        super(menu, key);
    }

    @Override
    protected void createDialog() {
        super.createDialog();
        SettingUtil settingUtil = AuroraApplication.getSettingUtil();
        String[] vals = settingUtil.getSelectorItems(context, key);
        int defVal = settingUtil.getDefaultRadioButtonValues(key);
        int val = PrefUtil.getInteger(key, defVal);
        dialog.setSingleChoiceItems(vals, val, (dialog, which) -> returnValue = which);
    }

    @Override
    protected View getView() {
        return null;
    }

    @Override
    protected DialogInterface.OnClickListener getOKEvent() {
        return (dialog1, which) -> {
            dialog1.dismiss();
            if(returnValue != null) {
                PrefUtil.putInteger(key, (int) returnValue);
                context.onConfigurationChanged(new Configuration());
            }
        };
    }

    @Override
    public View getItemView(ViewGroup group) {
        View view = super.getItemView(group);
        view.setOnClickListener((v) -> show());
        return view;
    }
}
