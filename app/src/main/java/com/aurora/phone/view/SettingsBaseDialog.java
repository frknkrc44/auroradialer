package com.aurora.phone.view;

import android.app.Activity;
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aurora.phone.AuroraApplication;
import com.aurora.phone.sheet.SettingsMenu;
import com.aurora.phone.util.SettingUtil;

public abstract class SettingsBaseDialog {
    protected AlertDialog.Builder dialog;
    protected Object returnValue;
    protected String key;
    protected Activity context;
    protected SettingsMenu menu;
    private View menuView;
    protected View itemView;

    protected SettingsBaseDialog(SettingsMenu menu, String key) {
        this.key = key;
        this.context = (Activity) menu.getContext();
        this.menu = menu;
        createDialog();
    }

    protected void createDialog() {
        if(dialog == null) {
            dialog = new AlertDialog.Builder(context);
            DialogInterface.OnClickListener cancelEvent = getCancelEvent();
            if(cancelEvent != null) {
                dialog.setNegativeButton(android.R.string.cancel, cancelEvent);
                dialog.setOnDismissListener((d) -> cancelEvent.onClick(d, 0));
            }
            if(itemView != null) {
                ((ViewGroup)itemView.getParent()).removeView(itemView);
            }
            View dialogView = itemView != null ? itemView : (itemView = getView());
            if(dialogView != null)
                dialog.setView(dialogView);
            DialogInterface.OnClickListener okEvent = getOKEvent();
            if(okEvent != null)
                dialog.setPositiveButton(android.R.string.ok, okEvent);
        }
    }

    protected abstract View getView();

    protected DialogInterface.OnClickListener getCancelEvent() {
        return (dialog, which) -> {
            dialog.cancel();
        };
    }

    protected abstract DialogInterface.OnClickListener getOKEvent();

    public void show() {
        dialog.show();
    }

    protected View getItemView(ViewGroup parent) {
        if(menuView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            menuView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            SettingUtil settingUtil = AuroraApplication.getSettingUtil();
            ((TextView) menuView.findViewById(android.R.id.text1)).setText(settingUtil.getTranslation(context, key));
        }
        return menuView;
    }
}
