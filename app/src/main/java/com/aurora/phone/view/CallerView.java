package com.aurora.phone.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.telecom.Call;
import android.telecom.Call.Details;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.aurora.contact.entity.Contact;
import com.aurora.phone.AuroraApplication;
import com.aurora.phone.R;
import com.aurora.phone.ViewActionHandler;
import com.aurora.phone.manager.CallManager;
import com.aurora.phone.util.Log;
import com.aurora.phone.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CallerView extends LinearLayout implements ViewActionHandler {

    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.line1)
    TextView line1;
    @BindView(R.id.line2)
    TextView line2;
    @BindView(R.id.line3)
    TextView line3;
    @BindView(R.id.img_volte)
    ImageView img_volte;
    @BindView(R.id.img_vowifi)
    ImageView img_vowifi;
    @BindView(R.id.img_voip)
    ImageView img_voip;
    @BindView(R.id.img_pause)
    ImageView img_pause;

    private Context context;
    private Call call;
    private Callback callback = new Callback();
    private Contact contact;
    
    // Add for testing
    private Handler timeHandler;

    public CallerView(Context context) {
        super(context);
    }

    public CallerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.call = AuroraApplication.getLastCall();
        this.contact = CallManager.getDisplayContact(context, call);
        this.timeHandler = new TimeHandler();

        View view = LayoutInflater.from(getContext()).inflate(R.layout.view_caller, this);
        ButterKnife.bind(this, view);

        img.setAnimation(AnimationUtils.loadAnimation(context, R.anim.anim_circular_grow));
        call.registerCallback(callback);
        setupDisplayContact();
    }

    @SuppressLint("SetTextI18n")
    public void setupDisplayContact() {
        Details details = call.getDetails();
        if (details.hasProperty(Details.PROPERTY_CONFERENCE)) {
            line2.setText("Conference Call");
            line3.setText("");
        } else {
            line2.setText(contact.getCompositeName());
            line3.setText(contact.getPhoneNumber());
        }
        
        img.setImageResource(details.hasProperty(Details.PROPERTY_CONFERENCE) 
                                ? R.drawable.ic_call_conference 
                                : details.hasProperty(Details.PROPERTY_NETWORK_IDENTIFIED_EMERGENCY_CALL) || details.hasProperty(Details.PROPERTY_EMERGENCY_CALLBACK_MODE)
                                    ? R.drawable.ic_call_emergency
                                    : R.drawable.ic_person);
        if (details.hasProperty(Details.PROPERTY_HIGH_DEF_AUDIO)) {
            img_volte.setVisibility(View.VISIBLE);
        }
        
        if (details.hasProperty(Details.PROPERTY_WIFI)) {
            img_vowifi.setVisibility(View.VISIBLE);
        }
        
        if (details.hasProperty(Details.PROPERTY_VOIP_AUDIO_MODE)) {
            img_voip.setVisibility(View.VISIBLE);
        }
    }

    public void updateUI(int state) {
        setupDisplayContact();
        switch (state) {
            case Call.STATE_ACTIVE:
                timeHandler.sendEmptyMessage(TimeHandler.MSG_UPDATE_TIME);
                break;
            case Call.STATE_DISCONNECTED:
                timeHandler.sendEmptyMessage(TimeHandler.MSG_STOP_UPDATE_TIME);
                // line2.setText("Disconnecting");
                break;
            case Call.STATE_RINGING:
                // line2.setText("Ringing");
                break;
            case Call.STATE_DIALING:
                // line2.setText("Dialing");
                break;
            case Call.STATE_CONNECTING:
                // line2.setText("Connecting");
                break;
            case Call.STATE_HOLDING:
                img_pause.setVisibility(View.VISIBLE);
                // line2.setText("On hold");
                break;
            default:
                break;
        }

        if (Call.STATE_HOLDING != state) {
            img_pause.setVisibility(View.GONE);
        }
    }

    @Override
    public void start() {
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void stop() {
        if (call != null)
            call.unregisterCallback(callback);
        img.clearAnimation();
    }


    public class Callback extends Call.Callback {
        @Override
        public void onStateChanged(Call call, int state) {
            super.onStateChanged(call, state);
            Log.e("CallerView : State changed -> %s", state);
            updateUI(state);
        }

        @Override
        public void onDetailsChanged(Call call, Call.Details details) {
            super.onDetailsChanged(call, details);
            Log.i("Details changed: %s", details.toString());
        }
    }
    
    @SuppressLint("HandlerLeak")
    private class TimeHandler extends Handler {
        private static final int MSG_UPDATE_TIME = 0, MSG_STOP_UPDATE_TIME = 1;
        
        @Override
        public void handleMessage(Message msg){
            switch(msg.what){
                case MSG_UPDATE_TIME:
                    if (call.getDetails().getConnectTimeMillis() > 0) {
                        long time = System.currentTimeMillis() - call.getDetails().getConnectTimeMillis();
                        line1.setText(Util.formatMillis(time));
                    }
                    removeMessages(MSG_UPDATE_TIME);
                    sendEmptyMessageDelayed(MSG_UPDATE_TIME, 500);
                    break;
                case MSG_STOP_UPDATE_TIME:
                    removeMessages(MSG_UPDATE_TIME);
                    removeMessages(MSG_STOP_UPDATE_TIME);
                    break;
            }
        }
        

    }
}
