package com.aurora.phone.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aurora.phone.AuroraApplication;
import com.aurora.phone.sheet.SettingsMenu;
import com.aurora.phone.util.SettingUtil;

public class SettingsIntent extends SettingsBaseDialog {

    public SettingsIntent(SettingsMenu menu, String key) {
        super(menu, key);
    }

    @Override
    protected void createDialog() {}

    @Override
    protected View getView() {
        return null;
    }

    @Override
    protected DialogInterface.OnClickListener getOKEvent() {
        return null;
    }

    @Override
    public View getItemView(ViewGroup parent) {
        View view = super.getItemView(parent);
        view.setOnClickListener((v) -> {
            SettingUtil settingUtil = AuroraApplication.getSettingUtil();
            Intent intent = settingUtil.getIntent(menu.getContext(), key);
            if(intent != null) {
                try {
                    menu.getDialog().dismiss();
                    menu.getContext().startActivity(intent);
                } catch (Throwable t) {
                    // do nothing
                }
            }
        });
        return view;
    }
}
